const SOURCE_PATH = './src';
const BUILD_PATH = './build';
const STATIC_PATH = './static';
const FAVICON_SOURCE = 'favicon.ico';
const TITLE = 'My settings webpack';
export const HOST = 'localhost';
export const PORT = 3000;



export {
    SOURCE_PATH,
    BUILD_PATH,
    STATIC_PATH,
    FAVICON_SOURCE,
    TITLE
}