import { path as PROJECT_ROOT } from 'app-root-path';
import { resolve } from 'path';
import {
    SOURCE_PATH,
    BUILD_PATH,
    STATIC_PATH
} from '../constants'

export const SOURCE = resolve(PROJECT_ROOT, SOURCE_PATH);
export const BUILD = resolve(PROJECT_ROOT, BUILD_PATH);
export const STATICS = resolve(PROJECT_ROOT, STATIC_PATH);