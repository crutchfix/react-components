import merge from 'webpack-merge';
import openBrowser from 'react-dev-utils/openBrowser';
import generateCommonConfiguration from './webpack.config.common';
import { loadDevelopmentCss } from '../modules/index';
import { choosePort } from 'react-dev-utils/WebpackDevServerUtils';
import { HOST, PORT } from '../../constants';

const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

export default async () => {
    const suggestedPort = await choosePort(HOST, PORT);
    setImmediate(() => {
        console.log(`http://localhost:${suggestedPort}`);
        openBrowser(`http://localhost:${suggestedPort}`);
    });

    return merge(
        generateCommonConfiguration(),
        loadDevelopmentCss(),
        {
            mode:   'development',
            output: {
                filename: 'js/[name].[hash:5].js',
            },
            devtool: 'cheap-module-eval-source-map',
            serve:   {
                host: HOST,
                port: suggestedPort,
            },
            plugins: [ new FriendlyErrorsWebpackPlugin() ],
        },
    );
};
