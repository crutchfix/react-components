import merge from 'webpack-merge';
import SimpleProgressWebpackPlugin from 'simple-progress-webpack-plugin';
import generateCommonConfiguration from './webpack.config.common';
import {
    loadProductionCss,
    setupBuildAnalysis,
    setupFavicon,
    cleanBuildDirectory,
} from '../modules';

export default () => merge(
    generateCommonConfiguration(),
    loadProductionCss(),
    //setupFavicon(),
    cleanBuildDirectory(),
    setupBuildAnalysis(),
    {
        mode:   'production',
        output: {
            filename: 'js/[name].[chunkhash:5].js',
        },
        plugins: [
            new SimpleProgressWebpackPlugin({
                format: 'compact',
            }),
        ],
    },
);