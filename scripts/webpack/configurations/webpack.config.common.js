import merge from 'webpack-merge';
import { SOURCE, BUILD } from '../paths';
import {
    loadJavaScript,
    loadFonts,
    loadImages,
    setupHtml,
    setupContextReplacement,
    initializeEnvVariables,
} from '../modules/index';

export default () => {
    const { NODE_ENV, DEPLOY_TARGET } = process.env;
    let REPOSITORY_NAME = '';

    return merge(
        loadJavaScript(),
        loadFonts(),
        loadImages(),
        setupHtml(),
        setupContextReplacement(),
        initializeEnvVariables({
            __ENV__:  JSON.stringify(NODE_ENV),
            __DEV__:  NODE_ENV === 'development',
            __PROD__: NODE_ENV === 'production',
        }),
        {
            entry: {
                SOURCE,
            },
            output: {
                path:       BUILD,
                publicPath: `${REPOSITORY_NAME}`,
            },
            resolve: {
                extensions: [
                    '.mjs',
                    '.js',
                    '.json',
                    '.css',
                    '.m.css',
                    '.png',
                    '.jpg',
                ],
                modules: [ SOURCE, 'node_modules' ],
            },
            optimization: {
                nodeEnv: NODE_ENV,
            },
            stats: false,
        },
    );
};
