import React from "react";
import ReactDOM from "react-dom";
import './theme/init';
const Index = () => {
    return <div id ="content">
                <header id="pageHeader">Header</header>
                <article id="mainArticle">Article</article>
                <nav id="mainNav">Nav</nav>
                <div id="siteAds">Ads</div>
                <footer id="pageFooter">Footer</footer>
            </div>;
};

ReactDOM.render(<Index />, document.getElementById("app"));